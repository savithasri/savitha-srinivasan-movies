package com.savitha.test;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.savitha.domain.Movie;
import com.savitha.service.MovieService;
import com.savitha.service.SequenceService;

/**
 * Hello world!
 *
 */
public class App {
  
  final static Logger logger = Logger.getLogger(App.class);
  static ApplicationContext context;
  
  @Test
  public static void main(String[] args) {
    context = (ApplicationContext) new ClassPathXmlApplicationContext("applicationContext.xml");
    movieTester();
    //movieGetTester();
  }

  public static void movieTester() {
    MovieService movieService = (MovieService) context.getBean("movieService");
    Movie movie = new Movie();
    movie.setName("Helper2");
    movie.setRating(6);
    movieService.addMovie(movie);
  }

  public static void movieGetTester() {
    MovieService movieService = (MovieService) context.getBean("movieService");
    Movie movie = movieService.getMoive(436);
    logger.debug("MovieId:"+movie.getMovieId()+",MovieName:"+movie.getName()+",MovieRating:"+movie.getRating());
  }
  
  public static void sequenceTester() {
    SequenceService sequenceService = (SequenceService) context.getBean("sequenceService");
    logger.debug("Next MovieId:"+sequenceService.getNextSequenceNo("MovieId"));
  }
}
