package com.savitha.service;

import java.util.List;

import com.savitha.domain.Movie;

public interface MovieService {
	List<Movie> findByRating(double d);
	void addMovie(Movie movie);
	Movie getMoive(Integer movieId);
	void updateMovie(Movie movie);
	void removeMovie(Integer movieId);
	void rateMovie(Movie movie);
  List<Movie> searchMoive(String search);
}
