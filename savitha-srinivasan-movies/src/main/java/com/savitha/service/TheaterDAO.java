package com.savitha.service;

import java.util.List;

import com.savitha.domain.Theater;

public interface TheaterDAO {
  List<Theater> findByPostalCode(double PostalCode);

  List<Theater> findByPostalCode(String PostalCode);

  void addTheater(Theater theater);

  void removeTheater(Integer theaterId);

  void updateTheater(Theater theater);

  Theater getTheater(Integer theaterId);
  

}
