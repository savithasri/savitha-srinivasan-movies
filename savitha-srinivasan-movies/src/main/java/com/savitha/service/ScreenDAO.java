package com.savitha.service;

import com.savitha.domain.Screen;

public interface ScreenDAO {
  public void addScreen(Screen screen);

  public void removeScreen(Integer screenId);

  public void updateScreen(Screen screen);

  public Screen getScreen(Integer screenId);
  
  
  /*Should we have list screen based on Theatername ?*/

}
