package com.savitha.service;

import java.util.List;

import com.savitha.domain.Movie;

public interface MovieDAO {
	List<Movie> findByRating(double minRating);
	public void addMovie(Movie movie);
	public void removeMovie(Integer movieId);
	public void updateMovie(Movie movie);
	public Movie getMovie(Integer movieId);
	public List<Movie> searchMoive(String search);
}
