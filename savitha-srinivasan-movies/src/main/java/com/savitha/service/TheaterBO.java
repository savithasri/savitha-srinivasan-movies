package com.savitha.service;

import java.util.List;

import com.savitha.domain.Theater;

public interface TheaterBO {

  List<Theater> findByPostalCode(double PostalCode);

  List<Theater> findByPostalCode(String PostalCode);

  void addTheater(Theater theater);

  void removeTheater(Integer theaterId);

  void updateTheater(Theater theater);

  //Theater getTheater(long theaterId);

  Theater getTheater(Integer theaterId);
	
}
