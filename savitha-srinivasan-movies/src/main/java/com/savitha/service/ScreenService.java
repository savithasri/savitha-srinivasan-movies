package com.savitha.service;

import com.savitha.domain.Screen;

public interface ScreenService {
	void addScreen(Screen screen);
	Screen getScreen(Integer screenId);
	void updateScreen(Screen screen);
	void removeScreen(Integer screenId);
}
