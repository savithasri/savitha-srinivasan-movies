package com.savitha.service.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.savitha.service.ScreenDAO;
import com.savitha.domain.Screen;


@Repository(value="hibernate")
public class ScreenHibernateDAOImpl implements ScreenDAO{

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	

	public void addScreen(Screen screen) {
		Session session = sessionFactory.getCurrentSession();
		session.save(screen);
	}

	
	
  @Override
  public void removeScreen(Integer screenId) {
    Session session=sessionFactory.getCurrentSession();
    Screen screen=this.getScreen(screenId);
    session.delete(screen);
    
  }
 
  @Override
  public Screen getScreen(Integer screenId) {
    Session session = sessionFactory.getCurrentSession();
    Screen screen = (Screen) session.get(com.savitha.domain.Screen.class, screenId);
    return screen;
  }


  @Override
  public void updateScreen(Screen screen) {
    Session session = sessionFactory.getCurrentSession();
    session.update(screen);
  }
	
}
