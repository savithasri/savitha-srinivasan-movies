package com.savitha.service.impl;
import java.util.Collections;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.savitha.service.MovieDAO;
import com.savitha.domain.Movie;


@Repository
public class MovieHibernateDAOImpl implements MovieDAO{

  private SessionFactory sessionFactory;

  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }


  @Override
  public List<Movie> findByRating(double minRating) {
    String hql = "FROM com.savitha.domain.Movie WHERE averageRating >= "+minRating;
    Query query = (Query) sessionFactory.getCurrentSession().createQuery(hql);
    List<Movie> movies = query.list();
    return movies;
  }

  public void addMovie(Movie movie) {
    Session session = sessionFactory.getCurrentSession();
    session.save(movie);
  }

  @Override
  public void removeMovie(Integer movieId) {
    Session session = sessionFactory.getCurrentSession();
    Movie movie = this.getMovie(movieId);
    session.delete(movie);
  }


  @Override
  public void updateMovie(Movie movie) {
    Session session = sessionFactory.getCurrentSession();
    System.out.println(movie);
    session.update(movie);
  }

  @Override
  public Movie getMovie(Integer movieId) {
    Session session = sessionFactory.getCurrentSession();
    Movie movie = (Movie) session.get(com.savitha.domain.Movie.class, movieId);
    return movie;
  }

  @Override
  public List<Movie> searchMoive(String search) {
    String hql = "FROM com.savitha.domain.Movie WHERE name like '%"+search+"%'";
    Query query = (Query) sessionFactory.getCurrentSession().createQuery(hql);
    @SuppressWarnings("unchecked")
    List<Movie> movies = Collections.checkedList(query.list(), Movie.class);
    //movies = query.list();
    return movies;  
  }
}
