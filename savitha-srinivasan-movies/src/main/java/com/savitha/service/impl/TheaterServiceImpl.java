package com.savitha.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.savitha.domain.Theater;
import com.savitha.service.TheaterBO;
import com.savitha.service.TheaterService;

@Service
@Transactional
public class TheaterServiceImpl implements TheaterService {

  
  private TheaterBO  theaterBO;

  public void setTheaterBO(TheaterBO theaterBO) {
    this.theaterBO = theaterBO;
  }

  public List<Theater> findByPostalCode(double PostalCode) {
    return theaterBO.findByPostalCode(PostalCode);
  }

  public List<Theater> findByPostalCode(String PostalCode) {
    return theaterBO.findByPostalCode(PostalCode);
  }

  public void addTheater(Theater theater) {
    theaterBO.addTheater(theater);
  }


  public void updateTheater(Theater theater) {
    theaterBO.updateTheater(theater);
  }

  public Theater getTheater(Integer theaterId) {
    return theaterBO.getTheater(theaterId);
  }

  @Override
  public void removeTheater(Integer theaterId) {
    theaterBO.removeTheater(theaterId);
  }



  
}
