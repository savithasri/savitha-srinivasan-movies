package com.savitha.service.impl;

import com.savitha.context.SpringApplicationContext;
import com.savitha.service.SequenceService;

public class SequenceServiceUtil  {
  
  private SequenceServiceUtil() {
  }
  

  public static Integer getNextSequenceNo(String name) {
    return getService().getNextSequenceNo(name);
  }
  
  private static SequenceService getService() {
    return (SequenceService) SpringApplicationContext.getBean("sequenceService");
  }

}
