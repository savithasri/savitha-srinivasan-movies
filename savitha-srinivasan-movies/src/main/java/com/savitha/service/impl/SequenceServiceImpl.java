package com.savitha.service.impl;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.savitha.service.SequenceBO;
import com.savitha.service.SequenceService;


@Service
@Transactional
public class SequenceServiceImpl implements SequenceService{

  private SequenceBO sequenceBO;

  public void setSequenceBO(SequenceBO sequenceBO) {
    this.sequenceBO = sequenceBO;
  }
  
  @Override
  public Integer getNextSequenceNo(String name) {
    return sequenceBO.getNextSequenceNo(name);
  }
  

}
