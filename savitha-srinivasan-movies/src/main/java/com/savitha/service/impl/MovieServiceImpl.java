package com.savitha.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import com.savitha.domain.Movie;
import com.savitha.service.MovieBO;
import com.savitha.service.MovieService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class MovieServiceImpl implements MovieService{

	@Autowired
	private MovieBO movieBO;
	
	public void setMovieBO(MovieBO movieBO) {
		this.movieBO = movieBO;
	}

	@Override
	public List<Movie> findByRating(double minRating) {
		return movieBO.findByRating(minRating);
	}

	@Override
	public void addMovie(Movie movie) {
		movieBO.addMovie(movie);
	}

	@Override
	public Movie getMoive(Integer movieId) {
		return movieBO.getMoive(movieId);
	}

	@Override
	public void updateMovie(Movie movie) {
		movieBO.updateMovie(movie);
	}

	@Override
	public void removeMovie(Integer movieId) {
		movieBO.removeMovie(movieId);
	}

	@Override
	public void rateMovie(Movie movie) {
		movieBO.rateMovie(movie);
		
	}

  @Override
  public List<Movie> searchMoive(String search) {
    return movieBO.searchMoive(search);
  }
	

}
