package com.savitha.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.savitha.service.ScreenBO;
import com.savitha.service.ScreenDAO;
import com.savitha.domain.Screen;

@Component
public class ScreenBOImpl implements ScreenBO {

	@Autowired
	private ScreenDAO screenDAO;


	public ScreenDAO getScreenDAO() {
    return screenDAO;
  }


  public void setScreenDAO(ScreenDAO screenDAO) {
		this.screenDAO = screenDAO;
	}


	@Override
	public void addScreen(Screen screen) {
		screenDAO.addScreen(screen);
	}

	@Override
	public Screen getScreen(Integer screenId) {
		return screenDAO.getScreen(screenId);
	}


	@Override
	public void updateScreen(Screen screen) {
		screenDAO.updateScreen(screen);
	}


	@Override
	public void removeScreen(Integer screenId) {
		screenDAO.removeScreen(screenId);
	}



}
