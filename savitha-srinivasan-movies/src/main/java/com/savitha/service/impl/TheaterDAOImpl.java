package com.savitha.service.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.savitha.domain.Theater;
import com.savitha.service.TheaterDAO;

public class TheaterDAOImpl implements TheaterDAO {

  private SessionFactory sessionFactory;

  /* Done */
  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  /*Rewrite as in betweem*/
  @Override
  public List<Theater> findByPostalCode(double PostalCode) {
    String hql = "FROM com.savitha.domain.Theater WHERE PostalCode >= "+PostalCode;
    Query query = (Query) sessionFactory.getCurrentSession().createQuery(hql);
    List<Theater> theaters = query.list();
    
    return theaters;
  }

  @Override

  public List<Theater> findByPostalCode(String PostalCode) {

    String hql = "FROM com.savitha.domain.Theater WHERE PostalCode ="+PostalCode; 
    Query query = (Query) sessionFactory.getCurrentSession().createQuery(hql);
    @SuppressWarnings("unchecked")
    List<Theater> theater = Collections.checkedList(query.list(), Theater.class);
    return theater;

  }

  @Override
  public Theater getTheater(Integer theaterId) {
    Session session = sessionFactory.getCurrentSession();
    Theater theater = (Theater) session.get(com.savitha.domain.Theater.class, theaterId);

    return theater;
  }

  @Override
  public void updateTheater(Theater theater) {
    Session session = sessionFactory.getCurrentSession();
    System.out.println(theater);
    session.update(theater);

  }

  @Override

  public void addTheater(Theater theater) {
    Session session = sessionFactory.getCurrentSession();
    session.save(theater);
  }

  @Override
 
  public void removeTheater(Integer theaterId) {
    Session session = sessionFactory.getCurrentSession();
    Theater theater = this.getTheater(theaterId);
    session.delete(theater);

  }

}
