package com.savitha.service.impl;

import javax.transaction.Transactional;

import com.savitha.domain.Screen;
import com.savitha.service.ScreenBO;
import com.savitha.service.ScreenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class ScreenServiceImpl implements ScreenService{

	@Autowired
	private ScreenBO screenBO;
	
	public void setScreenBO(ScreenBO screenBO) {
		this.screenBO = screenBO;
	}

	@Override
	public void addScreen(Screen screen) {
		screenBO.addScreen(screen);
	}

	@Override
	public Screen getScreen(Integer screenId) {
		return screenBO.getScreen(screenId);
	}

	@Override
	public void updateScreen(Screen screen) {
		screenBO.updateScreen(screen);
	}

	@Override
	public void removeScreen(Integer screenId) {
		screenBO.removeScreen(screenId);
	}


}
