package com.savitha.service.impl;

import java.util.List;

import com.savitha.domain.Theater;
import com.savitha.service.TheaterBO;
import com.savitha.service.TheaterDAO;

public class TheaterBOImpl implements TheaterBO {

  private TheaterDAO theaterDAO;

  public TheaterDAO getTheaterDAO() {
    return theaterDAO;
  }

  public void setTheaterDAO(TheaterDAO theaterDAO) {
    this.theaterDAO = theaterDAO;
  }

  @Override
  public List<Theater> findByPostalCode(double PostalCode) {
    
    return theaterDAO.findByPostalCode(PostalCode);
  }

  @Override
  public List<Theater> findByPostalCode(String City) {
    
    return theaterDAO.findByPostalCode(City);
    
  }

  @Override
  public Theater getTheater(Integer theaterId) {
    return theaterDAO.getTheater( theaterId);
  }

  @Override
  public void updateTheater(Theater theater) {
    theaterDAO.updateTheater(theater);

  }

  @Override
  public void addTheater(Theater theater) {
    theaterDAO.addTheater(theater);

  }

 
  @Override
  public void removeTheater(Integer theaterId) {
     theaterDAO.removeTheater(theaterId);

  }



}
