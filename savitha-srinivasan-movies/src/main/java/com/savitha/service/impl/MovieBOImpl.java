package com.savitha.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.savitha.service.MovieBO;
import com.savitha.service.MovieDAO;
import com.savitha.domain.Movie;

@Component
public class MovieBOImpl implements MovieBO {

  @Autowired
  private MovieDAO movieDAO;

  public void setMovieDAO(MovieDAO movieDAO) {
    this.movieDAO = movieDAO;
  }
  
  @Override
  public void addMovie(Movie movie) {
    movie.setMovieId(SequenceServiceUtil.getNextSequenceNo("MovieId"));
    movieDAO.addMovie(movie);
  }

  @Override
  public void removeMovie(Integer movieId) {
    movieDAO.removeMovie(movieId);
  }

  @Override
  public void updateMovie(Movie movie) {
    movieDAO.updateMovie(movie);
  }

  @Override
  public Movie getMoive(Integer movieId) {
    return movieDAO.getMovie(movieId);
  }

  @Override
  public void rateMovie(Movie movie) {
    movieDAO.updateMovie(movie);
  }

  @Override
  public List<Movie> findByRating(double minRating) {
    return movieDAO.findByRating(minRating);
  }

  @Override
  public List<Movie> searchMoive(String search) {
    List<Movie> movies = movieDAO.searchMoive(search);
    return movies;
  }

}
