package com.savitha.service.impl;

import com.savitha.service.SequenceBO;
import com.savitha.service.SequenceDAO;

public class SequenceBOImpl implements SequenceBO {
  private SequenceDAO  sequenceDAO;

  public void setSequenceDAO(SequenceDAO sequenceDAO) {
    this.sequenceDAO = sequenceDAO;
  }

  @Override
  public Integer getNextSequenceNo(String name) {
   
    return sequenceDAO.getNextSequenceNo(name);
  }

}
