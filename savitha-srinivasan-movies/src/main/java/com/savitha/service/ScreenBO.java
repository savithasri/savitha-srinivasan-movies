package com.savitha.service;

import com.savitha.domain.Screen;

public interface ScreenBO {
  void addScreen(Screen screen);
  void removeScreen(Integer screenId);
  void updateScreen(Screen screen);
  Screen getScreen(Integer screenId);
}
