package com.savitha.service;

import java.util.List;

import com.savitha.domain.Movie;

public interface MovieBO {

	List<Movie> findByRating(double minRating);
	void addMovie(Movie movie);
	Movie getMoive(Integer movieId);
	void updateMovie(Movie movie);
	void removeMovie(Integer movieId);
	void rateMovie(Movie movie);
	List<Movie> searchMoive(String search);
	//Sequence getNextSequenceNo
}
