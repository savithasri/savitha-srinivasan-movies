package com.savitha.service;

public interface SequenceService {
  Integer getNextSequenceNo(String name);
}
