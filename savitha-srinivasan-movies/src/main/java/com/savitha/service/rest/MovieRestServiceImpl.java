package com.savitha.service.rest;

import java.util.List;

import com.savitha.domain.Movie;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.savitha.context.SpringApplicationContext;
import com.savitha.service.MovieService;
import com.savitha.service.SequenceService;

@Service
@Path("/rest/movie")
public class MovieRestServiceImpl
{

  final static Logger logger = Logger.getLogger(MovieRestServiceImpl.class);

  private MovieService movieService;

  @GET
  @Path(value="/")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Movie> featuredMovies() 
  {
    movieService = (MovieService) SpringApplicationContext.getBean("movieService");
    List<Movie> movies = movieService.findByRating(8);
    return movies;
  }

  @GET
  @Path(value="/findByRating/{rating}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Movie> findByRating(@PathParam("rating")double rating) 
  {
    movieService = (MovieService) SpringApplicationContext.getBean("movieService");
    List<Movie> movies = movieService.findByRating(rating);
    logger.debug("Hello World"+movies.size());
    return movies;
  }

  @GET
  @Path(value="/{movieId}")
  @Produces(MediaType.APPLICATION_JSON)
  public Movie getMovie(@PathParam("movieId")int movieId)
  {
    movieService = (MovieService) SpringApplicationContext.getBean("movieService");
    Movie movie = movieService.getMoive(movieId);
    return movie;
  }


  @PUT
  @Path(value="/addMovie/{name}/{rating}")
  public Movie addMovie(@PathParam("name") String name,
      @PathParam("rating")double rating)
  {
    movieService = (MovieService) SpringApplicationContext.getBean("movieService");
    
    Movie newMovie=new Movie();
    newMovie.setName(name);
    newMovie.setRating(rating);  
    return newMovie;
  }

  @DELETE
  @Path(value="/deleteMovie/{movieId}")
  public void deleteMovie(@PathParam("movieId")Integer movieId)
  {
    movieService=(MovieService)SpringApplicationContext.getBean("movieService");
    movieService.removeMovie(movieId);
  }
  
  /*403 error*/
  @PUT
  @Path(value="/updateMovie/{movieId}")
  public void updateMovie(@PathParam("movieId") Integer movieId,
                            String name,
                            Double rating)
  {
    movieService=(MovieService)SpringApplicationContext.getBean("movieService");
    Movie newMovie = movieService.getMoive(movieId);
    newMovie.setName(name); 
    newMovie.setRating(rating);
    movieService.updateMovie(newMovie);
  }
}



