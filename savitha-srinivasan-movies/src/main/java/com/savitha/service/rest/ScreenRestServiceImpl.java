package com.savitha.service.rest;



import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;

import com.savitha.context.SpringApplicationContext;
import com.savitha.domain.Screen;
import com.savitha.domain.Theater;
import com.savitha.service.ScreenService;
import com.savitha.service.TheaterService;

@Service
@Path("/rest/screens")
public class ScreenRestServiceImpl {
  private ScreenService screenService;
  private TheaterService theaterService;

  
  @PUT
  @Path(value="/addScreens/{ScreenName}/{seats}/{TheaterId}")
  @Produces(MediaType.APPLICATION_JSON)
  
  /*Tested*/
  public Screen addScreens(
      @PathParam("ScreenName") String screenName,
      @PathParam("seats") int seats,
      @PathParam("TheaterId") int theaterId
      ){
    screenService=(ScreenService)SpringApplicationContext.getBean("screenService");
    theaterService = (TheaterService) SpringApplicationContext.getBean("theaterService");
    
    Screen newScreen = new Screen();
    newScreen.setScreenName(screenName);
    newScreen.setSeats(seats);
    Theater theater = theaterService.getTheater(theaterId);
    newScreen.setTheater(theater);
    screenService.addScreen(newScreen);
    return newScreen;
  }
  /*Tested*/
  @DELETE
  @Path(value="/removeScreens/{ScreenId}")
  @Produces(MediaType.APPLICATION_JSON)
  public void removeScreens(@PathParam("ScreenId")Integer  screenId)
  {
    screenService=(ScreenService)SpringApplicationContext.getBean("screenService");
    screenService.removeScreen(screenId);
  }

  /*404 error*/
  @PUT
  @Path(value="/updateScreens/{screenId}")
  public void updateScreens(
      @PathParam("screenId") Integer screenId,
      String screenName,
      Integer seats
      )
      {
        /*Update Screens based on ScreenId)*/
        screenService=(ScreenService)SpringApplicationContext.getBean("screenService");
        Screen screen = screenService.getScreen(screenId);
        screen.setScreenName(screenName);
        screen.setSeats(seats);
        screenService.updateScreen(screen);
      }

}



