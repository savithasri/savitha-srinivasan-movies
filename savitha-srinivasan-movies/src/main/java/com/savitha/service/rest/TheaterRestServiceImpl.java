package com.savitha.service.rest;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Service;

import com.savitha.context.SpringApplicationContext;
import com.savitha.domain.Theater;
import com.savitha.service.TheaterService;

@Service
@Path("/rest/theaters")
public class TheaterRestServiceImpl {
 
  private TheaterService theaterService;

  /*Tested*/
  @GET
  @Path(value="/findByPostalCode/{PostalCode}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<Theater>findByPostalCode(@PathParam("PostalCode") String postalCode){
    theaterService=(TheaterService)SpringApplicationContext.getBean("theaterService");
    List<Theater> theaters=theaterService.findByPostalCode(postalCode);
    return theaters;
  }
  
  /*Check path again*/
  @PUT
  @Path(value="/addTheater/{theaterName}")
  public Theater  addTheater(@PathParam("TheaterName") String theaterName)
    {
    theaterService=(TheaterService)SpringApplicationContext.getBean("theaterService");
    Theater newTheater=new Theater();
    newTheater.setTheaterName(theaterName);
    theaterService.addTheater(newTheater);
    return newTheater;
  }
  
  /* foreign key constraints fails if one tries to delete theater without deleting screens for the particular id*/
  /*Alternatively set the on delete cascade while creating screen table*/
  @DELETE 
  @Path(value="/removeTheater/{theaterId}")
  public void removeTheater(@PathParam ("theaterId")Integer theaterId)
  {
   theaterService=(TheaterService)SpringApplicationContext.getBean("theaterService");
   theaterService.removeTheater(theaterId);
  }  
}

