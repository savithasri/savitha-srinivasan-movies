package com.savitha.service;

public interface SequenceDAO {
  Integer getNextSequenceNo(String name);
}
