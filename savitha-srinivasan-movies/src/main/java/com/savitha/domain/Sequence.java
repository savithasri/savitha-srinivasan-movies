package com.savitha.domain;

public class Sequence {
  private String name;
  private Integer lastValue ;
  private Integer step;
  public String getName() {
    return name;
  }
  public Integer getLastValue() {
    return lastValue;
  }
  public Integer getStep() {
    return step;
  }
  public void setName(String name) {
    this.name = name;
  }
  public void setLastValue(Integer lastValue) {
    this.lastValue = lastValue;
  }
  public void setStep(Integer step) {
    this.step = step;
  } 
  
}
