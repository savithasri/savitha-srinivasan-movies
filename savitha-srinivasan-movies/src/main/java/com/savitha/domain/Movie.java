package com.savitha.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonManagedReference;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Movie implements Serializable {
  
    final static Logger logger = Logger.getLogger(Movie.class);
    private static final long serialVersionUID = 1L;
	
	private Integer movieId;
	private String name;
	
	private double averageRating;
	private Set<Screen> screens = new HashSet<Screen>(0);


  public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getMovieId() {
		return movieId;
	}
	public void setMovieId(Integer movieId) {
	  //this.movieId = SequenceServiceUtil.getNextSequenceNo("MovieId");-436 ->replaced by 4010
	  this.movieId = movieId;
	}
	
	public double getRating() {
		return averageRating;
	}
	public void setRating(double rating) {
		this.averageRating = rating;
	}

	@JsonManagedReference("movie-screen")
	public Set<Screen> getScreens() {
    return this.screens;
  }

  public void setScreens(Set<Screen> screens) {
    System.out.println("Set ScreenCalled");
    this.screens = screens;
  }
  
  @Override
  public int hashCode() {
    System.out.println("inside Movie.hashCode");
    int result = ((movieId == null) ? 0 : movieId.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(Object obj) {
    System.out.println("inside Movie.equals");
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Movie other = (Movie) obj;
    if (movieId == null) {
      if (other.movieId != null)
        return false;
    } else if (!movieId.equals(other.movieId))
      return false;
    return true;
  }
  

  @Override
  public String toString() {
    return "Movie [movieId=" + movieId + ", name=" + name + ", averageRating=" + averageRating + "]";
  }
	
}
