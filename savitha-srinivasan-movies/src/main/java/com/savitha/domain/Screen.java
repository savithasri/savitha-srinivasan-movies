package com.savitha.domain;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonManagedReference;

import com.savitha.domain.Movie;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Screen implements java.io.Serializable {

  private static final long serialVersionUID = -4375969725000616284L;
  private Integer screenId;
  private String screenName;
  private int seats;
  
  private Movie movie;
  private Theater theater;

  
  @JsonBackReference("movie-screen")
  public Movie getMovie() {
    return movie;
  }

  public void setMovie(Movie movie) {
    this.movie = movie;
  }

  public Screen() {
  }

  public Integer getScreenId() {
    return this.screenId;
  }

  public void setScreenId(Integer screenId) {
    this.screenId = screenId;
  }

  @JsonManagedReference("theater-screen")
  public Theater getTheater() {
    return this.theater;
  }

  public void setTheater(Theater theater) {
    this.theater = theater;
  }

  public String getScreenName() {
    return this.screenName;
  }

  public void setScreenName(String screenName) {
    this.screenName = screenName;
  }

  public int getSeats() {
    return this.seats;
  }

  public void setSeats(int seats) {
    this.seats = seats;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((screenId == null) ? 0 : screenId.hashCode());
    System.out.println("inside Screen.hashCode for screenId:"+screenId+":"+result);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    System.out.println("inside Screen.equals");
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Screen other = (Screen) obj;
    if (screenId == null) {
      if (other.screenId != null)
        return false;
    } else if (!screenId.equals(other.screenId))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Screen [screenId=" + screenId + ", screenName=" + screenName + ", seats=" + seats +  "]";
  }

}
