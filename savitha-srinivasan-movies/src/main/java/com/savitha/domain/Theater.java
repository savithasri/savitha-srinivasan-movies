package com.savitha.domain;

import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Theater implements java.io.Serializable {

  private static final long serialVersionUID = 6794320707579798178L;
  private Integer theaterId;
  private String theaterName;
  private Set<Screen> screens = new HashSet<Screen>(0);

 
  public Theater() {
  }

  public Integer getTheaterId() {
    return this.theaterId;
  }

  public void setTheaterId(Integer theaterId) {
    this.theaterId = theaterId;
  }

  public String getTheaterName() {
    return this.theaterName;
  }

  public void setTheaterName(String theaterName) {
    this.theaterName = theaterName;
  }

  @JsonBackReference("theater-screen")
  public Set<Screen> getscreens() {
    return this.screens;
  }
  
  public void setscreens(Set<Screen> screens) {
    this.screens = screens;
  }
  
  @Override
  public int hashCode() {
    System.out.println("inside Theater.hashCode");
    final int prime = 31;
    int result = 1;
    result = prime * result + ((theaterId == null) ? 0 : theaterId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    System.out.println("inside Theater.equals");
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Theater other = (Theater) obj;
    if (theaterId == null) {
      if (other.theaterId != null)
        return false;
    } else if (!theaterId.equals(other.theaterId))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Theater [theaterId=" + theaterId + ", theaterName=" + theaterName +  "]";
  }


}
