angular.module('movieApp',['ui.router','ngResource','movieApp.controllers','movieApp.services']);

angular.module('movieApp').config(function($stateProvider,$httpProvider){
    $stateProvider.state('movies',{
        url:'/movies',
        templateUrl:'/MovieTheater/app/partials/movies.html',
        controller:'MovieListController'
    }).state('findMovieByRating',{
        url:'/rating/:minRating/view',
        templateUrl:'/MovieTheater/app/partials/movies.html',
        controller:'MovieListByRatingController'
    }).state('viewMovie',{
       url:'/movies/:id/view',
       templateUrl:'/MovieTheater/app/partials/movie-view.html',
       controller:'MovieViewController'
    }).state('deleteMovie',{
    	url:'/movies/:'
    }
    		);
}).run(function($state){
   $state.go('findMovieByRating',{minRating:8});
});


