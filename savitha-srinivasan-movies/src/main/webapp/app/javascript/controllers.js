
angular.module('movieApp.controllers',[])
.controller('MovieListController',function($scope,$state,popupService,$window,Movie){

    $scope.movies=Movie.query();

    $scope.deleteMovie=function(movie){
        if(popupService.showPopup('Are you sure you want to delete this?')){
            movie.$delete(function(){
                $window.location.href='';
            });
        }
    }
})
.controller('MovieViewController',function($scope,$stateParams,Movie){

    $scope.movie=Movie.get({id:$stateParams.id});

})
.controller('MovieListByRatingController',function($scope,$state,$stateParams,popupService,$window,Movie){
     	$scope.movies=Movie.findByRating({minRating:$stateParams.minRating});
     	
        $scope.deleteMovie=function(movie){
            if(popupService.showPopup('Are you sure you want to delete this?')){
                movie.$delete(function(){
                    $window.location.href='';
                });
            }
        }
		//alert($stateParams);
  	}
);