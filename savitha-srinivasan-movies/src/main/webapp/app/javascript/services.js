angular.module('movieApp.services',[]).factory('Movie',function($resource){
    return $resource('/MovieTheater/rest/movie/:id',{id:'@_id'},{
        update: {
            method: 'PUT'
        },
	    findByRating: {
	        url: '/MovieTheater/rest/movie/findByRating/:minRating',
	        method: 'GET',
	        isArray: true
	    }/*,
	    deleteMovie:{
	    	url:'/deleteMovie/{movieId}',
	    	method:'DELETE'
	    }*/
    });
}).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});